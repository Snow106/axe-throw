// Fill out your copyright notice in the Description page of Project Settings.
#include "Weapon.h"
#include "KhaimeraKratos.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Math/Vector.h"

// Sets default values
AWeapon::AWeapon()
{
	PrimaryActorTick.bCanEverTick = true;
	this->SetActorTickEnabled(false);

	WeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("WeaponCollision"));
	SetRootComponent(WeaponCollision);

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetupAttachment(GetRootComponent());
	SkeletalMesh->WakeAllRigidBodies();
	
	BladeCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BladeCollision"));
	BladeCollision->SetupAttachment(GetRootComponent());

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->InitialSpeed = 0;
	ProjectileMovement->MaxSpeed = 10000;
	ProjectileMovement->ProjectileGravityScale = 0;
	ProjectileMovement->SetVelocityInLocalSpace(FVector(0,0,0));

	WeaponRotationComponent = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("WeaponRotation"));
	
	ThrowForce = 0;
	WeaponState = EWeaponState::EWS_Equipped;
	RecallSpeed = 1.5f;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	this->SetActorTickEnabled(false);
	BladeCollision->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::BladeCollisionOverlapBegin);
	BladeCollision->SetCollisionResponseToAllChannels(ECR_Overlap);
	BladeCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	BladeCollision->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	BladeCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

	WeaponCollision->SetCollisionResponseToAllChannels(ECR_Block);
	WeaponCollision->SetCollisionResponseToChannel(ECC_Destructible, ECR_Overlap);
	WeaponCollision->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	WeaponCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

	WeaponRotationComponent->RotationRate = FRotator(0);

	DeactivateCollision();
}

void AWeapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	FRotator Rotation = GetActorRotation();

	switch(WeaponState)
	{
		case EWeaponState::EWS_Thrown:
			//movement handled via projectile component
			break;
		case EWeaponState::EWS_Recalled:
			if (CurrentRecallStep < 1)
			{
				SetActorLocation(CalculateRecallPosition(ParentCharacter->GetMesh()->GetSocketLocation("weapon_rSocket"), CalculateOffsetWorldPos(ParentCharacter), RecallPosition, CurrentRecallStep));
				CurrentRecallStep += 1/RecallSpeed * DeltaSeconds;
			}
			else
			{
				Equip(ParentCharacter);
			}
			break;
		default:
			break;		
	}
}

void AWeapon::ActivateCollision()
{
	//WeaponCollision->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	//WeaponCollision->SetEnableGravity(true);
	//WeaponCollision->SetSimulatePhysics(true);
	
	BladeCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AWeapon::DeactivateCollision()
{
	BladeCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AWeapon::Equip(AKhaimeraKratos *Main)
{
	if (Main)
	{
		SetInstigator(Main);
		ParentCharacter = Main;

		this->SetActorTickEnabled(false);
		WeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);	
		WeaponCollision->SetSimulatePhysics(false);
		WeaponState = EWeaponState::EWS_Equipped;
		const USkeletalMeshSocket* RightHandSocket = Main->GetMesh()->GetSocketByName("weapon_rSocket");
		WeaponRotationComponent->RotationRate = FRotator(0);
	
		if (RightHandSocket)
		{
			RightHandSocket->AttachActor(this, Main->GetMesh());
		}
	}
}

void AWeapon::Throw(const FVector &Direction, AKhaimeraKratos* Main, const float Force, const float RotationRate)
{
	if (Main)
	{
		SetInstigator(Main);
		ParentCharacter = Main;
		this->SetActorTickEnabled(true);
		DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		ActivateCollision();
		SetActorRotation(Main->GetActorRotation());
		ThrowDirection = Direction;
		ThrowForce = Force;
		WeaponState = EWeaponState::EWS_Thrown;
		ThrowRotationRate = RotationRate * 360;
		ProjectileMovement->Velocity = Direction * ThrowForce;
		WeaponRotationComponent->RotationRate = FRotator(ThrowRotationRate * -1, 0 , 0);
		RotationAtThrow = GetActorQuat();
		//ProjectileMovement->ProjectileGravityScale = 1; //we'll want to scale this later in a non linear way, will feel better
	}
}

void AWeapon::BeginRecall(AKhaimeraKratos* Main, float Speed)
{
	if (Main)
	{
		SetInstigator(Main);
		ParentCharacter = Main;
		WeaponState = EWeaponState::EWS_Recalled;
		RecallSpeed = Speed;
		RecallPosition = GetActorLocation();
		CurrentRecallStep = 0;
		WeaponRotationComponent->RotationRate = FRotator(CalculateRotationsBack(ThrowRotationRate, RecallSpeed) * -1, 0 , 0);
	}
}

FVector AWeapon::CalculateRecallPosition(const FVector& ReturnPoint, const FVector& OffsetPoint, const FVector& PullPoint, float Time)
{
	float rt = 1 - Time; //remainder Time

	return (rt * rt * PullPoint) + (2 * rt * Time * OffsetPoint) + (Time * Time * ReturnPoint);
}

FVector AWeapon::CalculateOffsetWorldPos(const AKhaimeraKratos *Character)
{
	const float pointRotation = FMath::DegreesToRadians(Character->GetActorRotation().Yaw * -1);
	const float pointX = Character->WeaponOffset.X;
	const float pointY = Character->WeaponOffset.Y;

	const float worldY = (FMath::Cos(pointRotation) * pointY - FMath::Sin(pointRotation) * pointX) + Character->GetActorLocation().Y;
	const float worldX = (FMath::Cos(pointRotation) * pointX + FMath::Sin(pointRotation) * pointY) + Character->GetActorLocation().X;

	return FVector(worldX, worldY, Character->WeaponOffset.Z);
}

float AWeapon::CalculateRotationsBack(const float RotationRate, const float Speed)
{
	// The result of this will make rotation = 0 by the time weapon is finished recall.
	return 360/Speed - FMath::RadiansToDegrees(RotationAtThrow.AngularDistance(GetActorQuat())) / Speed;
}

void AWeapon::BladeCollisionOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	WeaponCollision->SetSimulatePhysics(false);
	WeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);	
	WeaponState = EWeaponState::EWS_Collided;
	UE_LOG(LogTemp, Warning, TEXT("Weapon Collided"));

	ProjectileMovement->SetVelocityInLocalSpace(FVector(0,0,0));
	ProjectileMovement->ProjectileGravityScale = 0;
	WeaponRotationComponent->RotationRate = FRotator(0);
}

void AWeapon::BladeCollisionOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}