// Fill out your copyright notice in the Description page of Project Settings.


#include "KhaimeraKratos.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Weapon.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"

AKhaimeraKratos::AKhaimeraKratos()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	EquippedWeapon = Cast<AWeapon>(UGameplayStatics::GetActorOfClass(AActor::GetWorld(), AWeapon::StaticClass()));
	ThrowForce = 100000;
	ThrowRotationRate = 100.f;
	RecallSpeed = 1.5f;
	CameraBoomArmLength = 300.0f;
	CameraBoomArmLengthReduction = 75.0f;
	CameraBoom->TargetArmLength = CameraBoomArmLength;
	CameraZoomRate = 2.0f;
	bCameraZooming = false;
}

void AKhaimeraKratos::BeginPlay()
{
	Super::BeginPlay();
	if (EquippedWeapon)
	{
		EquipWeapon(EquippedWeapon);
	}	
}

void AKhaimeraKratos::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//Camera Zoom when Aiming
	if (bCameraZooming)
	{
		if (CombatMode == ECombatMode::ECM_Aiming)
		{
			CameraZoom(-CameraBoomArmLengthReduction, DeltaSeconds);
		}
		if (CombatMode == ECombatMode::ECM_Normal)
		{
			CameraZoom(0, DeltaSeconds);
		}	
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AKhaimeraKratos::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings 
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	
	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &AKhaimeraKratos::Aim);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &AKhaimeraKratos::StopAiming);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AKhaimeraKratos::Attack);

	PlayerInputComponent->BindAxis("MoveForward", this, &AKhaimeraKratos::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AKhaimeraKratos::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AKhaimeraKratos::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AKhaimeraKratos::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AKhaimeraKratos::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AKhaimeraKratos::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AKhaimeraKratos::OnResetVR);
}


void AKhaimeraKratos::OnResetVR()
{
	// If ThirdPersonSandbox is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in ThirdPersonSandbox.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AKhaimeraKratos::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AKhaimeraKratos::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AKhaimeraKratos::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AKhaimeraKratos::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AKhaimeraKratos::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AKhaimeraKratos::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AKhaimeraKratos::EquipWeapon(AWeapon *Weapon)
{
	UE_LOG(LogTemp, Warning, TEXT("Equipping weapon"));
	Weapon->Equip(this);
}

void AKhaimeraKratos::Aim()
{
	CombatMode = ECombatMode::ECM_Aiming;
	bCameraZooming = true;
}

void AKhaimeraKratos::StopAiming()
{
	CombatMode = ECombatMode::ECM_Normal;
}

void AKhaimeraKratos::CameraZoom(float ZoomDistance, float DeltaTime)
{
	CameraBoom->TargetArmLength = FMath::FInterpTo(CameraBoom->TargetArmLength, CameraBoomArmLength + ZoomDistance, DeltaTime, CameraZoomRate);
	if (CameraBoom->TargetArmLength + ZoomDistance == CameraBoomArmLength)
	{
		bCameraZooming = false;
	}	
}

void AKhaimeraKratos::Attack()
{
	if (EquippedWeapon->WeaponState != EWeaponState::EWS_Equipped)
	{
		RecallWeapon(EquippedWeapon);
	}	
	else if (CombatMode == ECombatMode::ECM_Aiming)
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(CombatMontage, 1);
			AnimInstance->Montage_JumpToSection(FName("Aim"), CombatMontage);
		}
	}
	else
	{
		// Normal Attacks
	}
}

void AKhaimeraKratos::ThrowWeapon(AWeapon *Weapon)
{
	Weapon->Throw(CalculateThrowDirection(), this, ThrowForce, ThrowRotationRate);
}

void AKhaimeraKratos::RecallWeapon(AWeapon* Weapon)
{
	Weapon->BeginRecall(this, RecallSpeed);
}

FVector AKhaimeraKratos::CalculateThrowDirection() const
{
	return FollowCamera->GetForwardVector();
}
