// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"
UENUM(BlueprintType)
enum class EWeaponState : uint8
{
    EWS_Equipped UMETA(DisplayName = "Equipped"),
	EWS_Thrown UMETA(DisplayName = "Thrown"),
	EWS_Collided UMETA(DisplayName = "Collided"),
	EWS_Recalled UMETA(DisplayName = "Recalled"),

    EWS_MAX UMETA(DisplayName = "DefaultMax")
};

UCLASS()
class THIRDPERSONSANDBOX_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SkeletalMesh")
	class USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Collision")
	class UBoxComponent* WeaponCollision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Collision")
	class UBoxComponent* BladeCollision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	class AKhaimeraKratos* ParentCharacter;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thrown")
	class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thrown")
	class URotatingMovementComponent* WeaponRotationComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thrown")
	EWeaponState WeaponState;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thrown")
	bool bCollidedAfterThrown;

private:
	UPROPERTY()
	FVector ThrowDirection;

	UPROPERTY()
	float ThrowForce;

	UPROPERTY()
	float ThrowRotationRate;

	UPROPERTY()
	FVector RecallPosition;

	UPROPERTY()
	float CurrentRecallStep;

	UPROPERTY()
	float RecallSpeed;

	UPROPERTY()
	FQuat RotationAtThrow;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

public:	
	UFUNCTION(BlueprintCallable)
	void ActivateCollision();

	UFUNCTION(BlueprintCallable)
	void DeactivateCollision();

	void Equip(AKhaimeraKratos *Main);

	void Throw(const FVector& Direction, AKhaimeraKratos *Main, const float Force, const float RotationRate);

	/**
	 *
	 */
	void BeginRecall(AKhaimeraKratos *Main, float Speed);

	UFUNCTION()
	void BladeCollisionOverlapBegin (UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void BladeCollisionOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	/**
	* Calculates the current position the weapon should be in while it is being recalled.
	* @param ReturnPoint point where the weapon should end in (hand)
	* @param OffsetPoint used to get a bezier curve when returning
	* @param PullPoint point where the weapon is when recall begins
	* @param Time "step" of the current position. Range from 0-1.
	*/
	static FVector CalculateRecallPosition(const FVector& ReturnPoint, const FVector& OffsetPoint, const FVector& PullPoint, float Time);

	static FVector CalculateOffsetWorldPos(const AKhaimeraKratos *Character);

	float CalculateRotationsBack(const float RotationRate, const float Speed);
};
