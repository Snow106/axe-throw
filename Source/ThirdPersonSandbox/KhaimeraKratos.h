// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "KhaimeraKratos.generated.h"

UENUM(BlueprintType)
enum class ECombatMode : uint8
{
	ECM_Normal UMETA(DisplayName = "Normal"),
    ECM_Aiming UMETA(DisplayName = "Aiming"),

    ECM_MAX UMETA(DisplayName = "DefaultMAX")
};

UCLASS()
class THIRDPERSONSANDBOX_API AKhaimeraKratos : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AKhaimeraKratos();


	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items)
	class AWeapon* EquippedWeapon;

	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = "true"))
	FVector WeaponOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Items)
	float ThrowForce;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Thrown")
	float RecallSpeed;

	/**
	* Amount of rotations when then the weapon is thrown/recalled
	* @param Rotations per second
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Thrown")
	float ThrowRotationRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Enums")
	ECombatMode CombatMode;

	UPROPERTY(EditAnywhere, BLueprintReadWrite, Category = Camera)
	float CameraBoomArmLength;

	UPROPERTY(EditAnywhere, BLueprintReadWrite, Category = Camera)
	float CameraBoomArmLengthReduction;
	
	UPROPERTY(EditAnywhere, BLueprintReadWrite, Category = Camera)
	float CameraZoomRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	bool bCameraZooming;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Anims")
	class UAnimMontage* CombatMontage;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value); 

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);
	
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	FORCEINLINE void SetEquippedWeapon(AWeapon* Weapon) { EquippedWeapon = Weapon; }
	FORCEINLINE AWeapon* GetEquippedWeapon() const { return EquippedWeapon; }

	void EquipWeapon(AWeapon *Weapon);

	UFUNCTION(BlueprintCallable)
	void ThrowWeapon(AWeapon *Weapon);

private:
	void Aim();
	void StopAiming();
	void CameraZoom(float ZoomDistance, float DeltaTime);
	void RecallWeapon(AWeapon *Weapon);
	void Attack();
	
	FVector CalculateThrowDirection() const;
};
