# Axe Throw

Inspired by God of War, I tried to recreate the core axe-throwing mechanic.

![Preview](https://i.imgur.com/aSXf53m.mp4)

Keep in mind that this was mostly a practice project for myself, so some bugs are lingering. For example, axe recall is a bit weird if the player rotates in place.
